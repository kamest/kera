package dev.kamest.kera.evaluators.error;

import com.google.common.util.concurrent.AtomicDouble;
import dev.kamest.kera.evaluators.Evaluator;
import lombok.extern.apachecommons.CommonsLog;

import java.util.Map;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Root mean square error
 * <p>
 * Refs:
 * https://en.wikipedia.org/wiki/Root-mean-square_deviation
 * <p>
 * This code is part of bachelor thesis:
 * Application of machine learning algorithms on purchase recommendation
 * UHK FIM KIT
 *
 * @author Štěpán Kamenik (kamenik.stepan@gmail.com / stepan.kamenik@uhk.cz), UHK FIM (c) 2020
 **/
@CommonsLog
public class RMSE extends Evaluator {

    @Override
    public double eval(Map<String, Map<String, Double>> testSet, Map<String, Map<String, Double>> recommendations) {

        AtomicDouble result = new AtomicDouble(0.0);
        AtomicInteger testSize = new AtomicInteger(0);

        testSet.forEach((i, j) -> {

            Map<String, Double> uVec = recommendations.get(i);
            if (uVec == null) {
                log.warn("user not found : " + i);
                return;
            }
            j.forEach((k, l) -> {
                Optional.ofNullable(uVec.get(k)).ifPresent(p -> {
                    result.addAndGet(Math.pow(l - p, 2));
                    testSize.incrementAndGet();
                });
            });

        });

        return Math.sqrt(result.get() / testSize.get());
    }
}
