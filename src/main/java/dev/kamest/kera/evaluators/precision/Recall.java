package dev.kamest.kera.evaluators.precision;

import com.google.common.util.concurrent.AtomicDouble;
import dev.kamest.kera.evaluators.Evaluator;

import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

/**
 * Recall
 *
 * Refs:
 * https://en.wikipedia.org/wiki/Evaluation_measures_(information_retrieval)#Recall
 * https://en.wikipedia.org/wiki/Precision_and_recall
 * https://developers.google.com/machine-learning/crash-course/classification/precision-and-recall
 * https://towardsdatascience.com/precision-vs-recall-386cf9f89488
 * <p>
 * This code is part of bachelor thesis:
 * Application of machine learning algorithms on purchase recommendation
 * UHK FIM KIT
 *
 * @author Štěpán Kamenik (kamenik.stepan@gmail.com / stepan.kamenik@uhk.cz), UHK FIM (c) 2020
 **/
public class Recall extends Evaluator {

    @SuppressWarnings("SimplifyStreamApiCallChains")
    @Override
    public double eval(Map<String, Map<String, Double>> testSet, Map<String, Map<String, Double>> recommendations) {
        AtomicDouble sumPrec = new AtomicDouble(0.0);
        AtomicInteger evals = new AtomicInteger();
        testSet.forEach((key, value) -> {
            List<String> strings = value.entrySet().stream().map(Map.Entry::getKey).collect(Collectors.toList());
            strings.retainAll(recommendations.get(key).entrySet().stream()
                    .sorted(Collections.reverseOrder(Comparator.comparingDouble(Map.Entry::getValue))).limit(getTopN()).map(Map.Entry::getKey).collect(Collectors.toList()));
            sumPrec.addAndGet(strings.size() / (value.size() + 0.0));
            evals.getAndIncrement();
        });

        return sumPrec.get() / evals.get();
    }
}
