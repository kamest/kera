package dev.kamest.kera.evaluators.precision;

import com.google.common.util.concurrent.AtomicDouble;
import dev.kamest.kera.evaluators.Evaluator;

import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

/**
 * Mean average precision
 *
 * Refs:
 * https://en.wikipedia.org/wiki/Evaluation_measures_(information_retrieval)#Mean_average_precision
 * https://medium.com/@jonathan_hui/map-mean-average-precision-for-object-detection-45c121a31173
 * <p>
 * This code is part of bachelor thesis:
 * Application of machine learning algorithms on purchase recommendation
 * UHK FIM KIT
 *
 * @author Štěpán Kamenik (kamenik.stepan@gmail.com / stepan.kamenik@uhk.cz), UHK FIM (c) 2020
 **/
public class MAP extends Evaluator {

    @Override
    public double eval(Map<String, Map<String, Double>> testSet, Map<String, Map<String, Double>> recommendations) {
        AtomicDouble sumPrec = new AtomicDouble(0.0);
        AtomicInteger evals = new AtomicInteger();


        testSet.forEach((key1, value) -> {
            int testSetSize = value.keySet().size();
            if (testSetSize > 0) {

                // Sort as for user
                List<Map.Entry<String, Double>> recommendationsForUser = recommendations
                        .get(key1)
                        .entrySet()
                        .stream()
                        .sorted(Collections.reverseOrder(Comparator.comparingDouble(Map.Entry::getValue)))
                        .collect(Collectors.toList());

                int numHits = 0;
                int topK = Math.min(getTopN(), recommendationsForUser.size());
                double curPrec = 0.0d;
                for (int i = 0; i < topK; ++i) {
                    if (value.containsKey(recommendationsForUser.get(i).getKey())) {
                        numHits++;
                        curPrec += 1.0 * numHits / (i + 1);
                    }
                }
                if (topK != 0) {
                    sumPrec.addAndGet( curPrec / (Math.min(testSetSize, topK)));
                    evals.getAndIncrement();
                }
            }
        });
        return sumPrec.get() / evals.get();
    }

}
