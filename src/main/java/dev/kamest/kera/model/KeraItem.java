package dev.kamest.kera.model;

import lombok.EqualsAndHashCode;
import lombok.Getter;

import java.util.LinkedList;
import java.util.List;

/**
 * Stands as product, movie or service in external service,
 * means this is the item for which user should create opinion.
 *
 * <p>
 * This code is part of bachelor thesis:
 * Application of machine learning algorithms on purchase recommendation
 * UHK FIM KIT
 *
 * @author Štěpán Kamenik (kamenik.stepan@gmail.com / stepan.kamenik@uhk.cz), UHK FIM (c) 2020
 **/
@EqualsAndHashCode(callSuper = true)
public class KeraItem extends IdProvider implements KeraExtObject {

    public KeraItem(String externalId) {
        super(externalId);
    }

    public KeraItem(String externalId,List<String> tags,String name,String masterId) {
        super(externalId);
        this.tags = tags;
        this.name = name;
        this.masterId = masterId;
    }
    public KeraItem(KeraItem item) {
        super(item.getExternalId());
        this.tags = item.getTags();
        this.name = item.getName();
        this.masterId = item.getMasterId();
    }

    @Getter
    private List<String> tags = new LinkedList<>();

    @Getter
    private String name;

    @Getter
    private String masterId;
}
