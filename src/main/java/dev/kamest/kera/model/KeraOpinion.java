package dev.kamest.kera.model;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * Basic object that has opinion from user to item at some "score".
 *
 * System indicates two main types,
 * rating : at some scale 1-10,1-5,1-100
 * ranking : 0 or 1 , one syndicates bought, visited, clicked and zero syndicates unknown state
 *
 * <p>
 * This code is part of bachelor thesis:
 * Application of machine learning algorithms on purchase recommendation
 * UHK FIM KIT
 *
 * @author Štěpán Kamenik (kamenik.stepan@gmail.com / stepan.kamenik@uhk.cz), UHK FIM (c) 2020
 **/
@Data
@AllArgsConstructor
public class KeraOpinion implements KeraExtObject {

    private String userId;
    private String itemId;
    private Double opinion;

}
