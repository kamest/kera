package dev.kamest.kera.model;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

/**
 * DTO that holds information for external providers which
 * products / movies / services should be recommended,
 * is mainly output of recommend method in {@link dev.kamest.kera.alg.KeraComputer}
 * <p>
 * This code is part of bachelor thesis:
 * Application of machine learning algorithms on purchase recommendation
 * UHK FIM KIT
 *
 * @author Štěpán Kamenik (kamenik.stepan@gmail.com / stepan.kamenik@uhk.cz), UHK FIM (c) 2020
 **/
@EqualsAndHashCode(callSuper = true)
public class KeraResultItem extends KeraItem {

    @Getter
    @Setter
    private Double score;

    public KeraResultItem(KeraItem item, Double score) {
        super(item);
        this.score = score;
    }

}
