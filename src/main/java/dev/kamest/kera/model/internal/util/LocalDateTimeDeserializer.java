package dev.kamest.kera.model.internal.util;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;

import java.io.IOException;
import java.time.LocalDateTime;

/**
 * Simple deseralizer of {@link LocalDateTime}.
 * <p>
 * This code is part of bachelor thesis:
 * Application of machine learning algorithms on purchase recommendation
 * UHK FIM KIT
 *
 * @author Štěpán Kamenik (kamenik.stepan@gmail.com / stepan.kamenik@uhk.cz), UHK FIM (c) 2020
 **/
public class LocalDateTimeDeserializer extends StdDeserializer<LocalDateTime> {

    public LocalDateTimeDeserializer() {
        super(LocalDateTime.class);
    }

    @Override
    public LocalDateTime deserialize(JsonParser p, DeserializationContext ctxt) throws IOException {
        return LocalDateTime.parse(p.getValueAsString());
    }
}
