package dev.kamest.kera.model.internal;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import lombok.experimental.Delegate;

import java.util.HashMap;
import java.util.Map;

/**
 * Main class that contains result of most computers.
 * <p>
 * Mostly is designed as <UserId,<ItemId,Score>>,
 * or in case of basic statistical methods only
 * <0,<ItemId,Score>>.
 * <p>
 * In future this class could contain some highly used
 * operations on this matrix.
 * <p>
 * This code is part of bachelor thesis:
 * Application of machine learning algorithms on purchase recommendation
 * UHK FIM KIT
 *
 * @author Štěpán Kamenik (kamenik.stepan@gmail.com / stepan.kamenik@uhk.cz), UHK FIM (c) 2020
 **/
@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class KeraMatrixModel {

    @Delegate
    Map<Integer, Map<Integer, Double>> matrix = new HashMap<>();


}
