package dev.kamest.kera;

import Jama.Matrix;
import com.fasterxml.jackson.databind.ObjectMapper;
import dev.kamest.kera.alg.KeraComputer;
import dev.kamest.kera.model.KeraItem;
import dev.kamest.kera.model.KeraResultItem;
import dev.kamest.kera.model.internal.KeraComputerData;
import dev.kamest.kera.prov.ext.ItemsProvider;
import dev.kamest.kera.prov.ext.OpinionsProvider;
import dev.kamest.kera.prov.ext.UsersProvider;
import lombok.*;
import lombok.extern.apachecommons.CommonsLog;
import org.apache.commons.io.FileUtils;
import org.apache.commons.math3.linear.ArrayRealVector;
import org.apache.commons.math3.linear.MatrixUtils;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static java.util.Optional.ofNullable;

/**
 * MAIN SERVICE of this whole library.
 * <p>
 * Holds all running computers, and provides all basic
 * interactions to library from external system.
 * <p>
 * In case that you needs to implement some advanced functions,
 * Kera provides all necessary methods to go throw tree of the library.
 * Kera
 * - Computers
 * - Evaluators
 * --Providers
 * ---FinalObjects
 *
 *
 * <p>
 * This code is part of bachelor thesis:
 * Application of machine learning algorithms on purchase recommendation
 * UHK FIM KIT
 *
 * @author Štěpán Kamenik (kamenik.stepan@gmail.com / stepan.kamenik@uhk.cz), UHK FIM (c) 2020
 **/
@RequiredArgsConstructor
@Data
@CommonsLog
public class Kera {
    /**
     * Path in which are strored models trained in each computers. (Could be really large)
     */
    private static final String BASE_DATA_MODEL_PATH = "data/models/";

    /**
     * @see ItemsProvider
     */
    private final ItemsProvider itemsProvider;
    /**
     * @see UsersProvider
     */
    private final UsersProvider usersProvider;
    /**
     * @see OpinionsProvider
     */
    private final OpinionsProvider opinionsProvider;

    /**
     * List of all running computers / recommenders.
     */
    @Setter(AccessLevel.NONE)
    @Getter(AccessLevel.NONE)
    private Map<String, KeraComputer> computers = new HashMap<>();

    private ObjectMapper mapper = new ObjectMapper();

    /**
     * Recomputes all models in computers,
     * should be executed only in night or in low traffic,
     * could be really CPU hungry and is sometimes slow.
     */
    public void recompute() {
        computers.forEach((computerCode, j) -> recomputeOne(computerCode));
    }

    /**
     * Adds computer with specific code,
     * if there isn't stored model, retrain immediately
     * @param computerCode unique code of computer
     * @param computer computer itself @see {@link KeraComputer}
     */
    public void addComputer(String computerCode, KeraComputer computer) {
        computers.put(computerCode, computer);
        // Try to read model of drive
        KeraComputerData matrixModel = readModel(computerCode);
        if (matrixModel != null) {
            // model found set values to computer
            computer.initVars(itemsProvider, usersProvider, opinionsProvider);
            computer.setModel(matrixModel.getModel());

            computer.setItemIndex(matrixModel.getItemIndex());
            computer.setUserIndex(matrixModel.getUserIndex());
            computer.setTagIndex(matrixModel.getTagIndex());
            computer.setItemExtIndex(matrixModel.getItemIndex().entrySet().stream().collect(Collectors.toMap(Map.Entry::getValue, Map.Entry::getKey)));
            computer.setUserExtIndex(matrixModel.getUserIndex().entrySet().stream().collect(Collectors.toMap(Map.Entry::getValue, Map.Entry::getKey)));
            computer.setTagExtIndex(matrixModel.getTagIndex().entrySet().stream().collect(Collectors.toMap(Map.Entry::getValue, Map.Entry::getKey)));

            computer.setModelAdditionalData(matrixModel.getModelAdditionalData());

            if (matrixModel.getUserMatrix() != null){
                computer.setUserMatrix(new Matrix(matrixModel.getUserMatrix()));
                computer.setItemMatrix(new Matrix(matrixModel.getItemMatrix()));
                computer.setWeight(new ArrayRealVector(matrixModel.getWeight()));

                computer.setItemBias(matrixModel.getItemBias());
                computer.setUserBias(matrixModel.getUserBias());
                computer.setGlobalBias(matrixModel.getGlobalBias());
            }
        } else {
            // model not found retrain
            recomputeOne(computerCode);
        }
    }

    /**
     * Adds computer and forces to recompute without knowing if model is actually stored.
     *
     * @param computerCode unique code of computer
     * @param computer     computer itself @see {@link KeraComputer}
     */
    public void addComputerWithComputation(String computerCode, KeraComputer computer) {
        computers.put(computerCode, computer);
        recomputeOne(computerCode);
    }

    /**
     * Recomputes computers model.
     *
     * @param computerCode unique code of computer
     */
    public void recomputeOne(String computerCode) {
        KeraComputer computer = computers.get(computerCode);
        ofNullable(computer).orElseThrow(() -> new IllegalArgumentException("Computer not present in Kera!"));

        computer.initVars(itemsProvider, usersProvider, opinionsProvider);
        computer.computeModel();

        storeModel(new KeraComputerData(computer), computerCode);
    }

    /**
     * Basic method for recommendation,
     * recommends best n items for user userId
     *
     * @param userId           user for whom we create recommendations
     * @param n                number of recommended items
     * @param forbidKnownItems indicates whether known (referenced and history items) could be recommended.
     * @return list of recommended items
     */
    public List<KeraResultItem> recommend(String computerCode, String userId, int n, boolean forbidKnownItems) {
        return getComputer(computerCode).recommend(userId, n, forbidKnownItems);
    }

    /**
     * Recommends with referenced items, mostly fins similar to referenced items, not only for history opinions.
     *
     * @param userId           user for whom we create recommendations
     * @param n                number of recommended items
     * @param referenceItems   items that should recommends be similar to.
     * @param forbidKnownItems indicates whether known (referenced and history items) could be recommended.
     * @return list of recommended items
     */
    public List<KeraResultItem> recommend(String computerCode, String userId, int n, List<KeraItem> referenceItems, boolean forbidKnownItems) {
        return getComputer(computerCode).recommend(userId, n, referenceItems, forbidKnownItems);
    }

    /**
     * Get specific computer by code.
     *
     * @param computerCode unique code of computer
     * @return requested computer
     */
    public KeraComputer getComputer(String computerCode) {
        return computers.get(computerCode);
    }

    /**
     * Read model from drive and deserialize.
     *
     * @param computerCode unique code of computer
     * @return stored computer data
     */
    private KeraComputerData readModel(String computerCode) {
        try {
            return mapper.readValue(new File(BASE_DATA_MODEL_PATH + computerCode + ".json"), KeraComputerData.class);
        } catch (Exception e) {
            // ignored and model will be recomputed
//            e.printStackTrace();
        }
        log.info("Saved model not found or corrupted, recomputing new one instance.");
        return null;

    }

    /**
     * Stores computers data on drive.
     *
     * @param data         data to be stored on drive
     * @param computerCode unique code of computer
     */
    private void storeModel(KeraComputerData data, String computerCode) {
        try {
            File file = new File(BASE_DATA_MODEL_PATH + computerCode + ".json");
            FileUtils.touch(file);
            mapper.writeValue(file, data);
        } catch (IOException e) {
            throw new IllegalStateException("Cannot store model for computer: " + computerCode);
        }

    }

}
