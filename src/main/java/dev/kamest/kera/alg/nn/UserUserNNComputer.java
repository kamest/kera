package dev.kamest.kera.alg.nn;

import com.google.common.util.concurrent.AtomicDouble;
import dev.kamest.kera.alg.KeraComputer;
import dev.kamest.kera.alg.VectorUtils;
import dev.kamest.kera.model.IdProvider;
import dev.kamest.kera.model.KeraItem;
import dev.kamest.kera.model.KeraResultItem;
import dev.kamest.kera.model.KeraUser;
import dev.kamest.kera.model.internal.KeraMatrixModel;
import lombok.extern.apachecommons.CommonsLog;

import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

/**
 * Basic Item-Item based recommendation computer,
 * uses cosine similarity, but that could be easily replaced with other method (Pearson,etc..)
 * <p>
 * Look at the methods doc.
 * <p>
 * Refs:
 * https://grouplens.org/blog/similarity-functions-for-user-user-collaborative-filtering/
 * https://medium.com/sfu-cspmp/recommendation-systems-user-based-collaborative-filtering-using-n-nearest-neighbors-bf7361dc24e0
 * <p>
 * This code is part of bachelor thesis:
 * Application of machine learning algorithms on purchase recommendation
 * UHK FIM KIT
 *
 * @author Štěpán Kamenik (kamenik.stepan@gmail.com / stepan.kamenik@uhk.cz), UHK FIM (c) 2020
 **/
@CommonsLog
public class UserUserNNComputer extends KeraComputer {

    public UserUserNNComputer(boolean isRanking) {
        super(isRanking);
    }

    public static boolean needsRegisteredUsers() {
        return true;
    }

    /**
     * @see UserUserNNComputer#recommend(java.lang.String, int, java.util.List, boolean)
     */
    @Override
    public List<KeraResultItem> recommend(String userId, int n, boolean forbidKnownItems) {
        return recommend(userId, n, Collections.emptyList(), forbidKnownItems);
    }

    /**
     * In this method everything is already pre-computed in matrix,
     * so in recommending we could only retrieve predicted values from matrix to current user and
     * remove items that are already purchased or referenced, only in case of forbidKnownItems
     *
     * @param userId           user for whom we create recommendations
     * @param n                number of recommended items
     * @param referenceItems   items that user already choose
     * @param forbidKnownItems indicates whether known (referenced and history items) could be recommended.
     * @return list of recommended items
     */
    @Override
    public List<KeraResultItem> recommend(String userId, int n, List<KeraItem> referenceItems, boolean forbidKnownItems) {
        Map<Integer, Double> userMap = getModel().get(getUserId(userId));
        if (userMap == null) return Collections.emptyList();
        List<String> forbiddenItems = getForbiddenItems(userId, referenceItems, forbidKnownItems);
        List<String> usedMasters = new LinkedList<>();
        Map<String, KeraItem> itemMap = itemsProvider.getObjects().stream().collect(Collectors.toMap(IdProvider::getExternalId, i -> i));
        return userMap
                .entrySet()
                .stream()
                .map(i -> new KeraResultItem(itemMap.get(getItemId(i.getKey())), i.getValue()))
                .filter(i -> {
                    if (!forbiddenItems.contains(i.getExternalId())){
                        String masterId = i.getMasterId();
                        if (masterId == null) return true;
                        if (usedMasters.contains(masterId)) return false;
                        usedMasters.add(masterId);
                        return true;
                    }
                    return false;
                })
                .sorted(Collections.reverseOrder(Comparator.comparingDouble(KeraResultItem::getScore)))
                .limit(n)
                .collect(Collectors.toList());
    }


    /**
     * Computes model that contains users x items matrix,
     * for each user computes similarity score with each other user and
     * picks top similar users and from them picks most accurate items.
     * <p>
     * Weight that items in case of rating and store i matrix.
     *
     * @param isRanking indicates whether normalization should be applied
     * @return trained matrix model
     */
    @Override
    public KeraMatrixModel computeModel(Boolean isRanking) {
        Map<Integer, Map<Integer, Double>> usersOpinions = new HashMap<>();

        //Create indexes
        opinionsProvider.getObjects().forEach(o -> {
            String userId = o.getUserId();
            Map<Integer, Double> userOpinions = usersOpinions.getOrDefault(addUserToIndex(userId), new HashMap<>());
            userOpinions.put(addItemToIndex(o.getItemId()), o.getOpinion());
            usersOpinions.put(getUserId(userId), userOpinions);
        });

        List<KeraItem> items = itemsProvider.getObjects();
        List<KeraUser> users = needsRegisteredUsers()
                ? usersProvider.getObjects().stream().filter(KeraUser::isRegistered).collect(Collectors.toList())
                : usersProvider.getObjects();
        KeraMatrixModel model = new KeraMatrixModel();

        AtomicInteger actUsersCount = new AtomicInteger(0);
        int size = users.size();
        users.forEach(user -> {
            int itemUsersIndex = actUsersCount.incrementAndGet();
            if (itemUsersIndex % 100 == 0) log.info("items " + itemUsersIndex + " from " + size);
            Integer userIndex = addUserToIndex(user.getExternalId());
            Map<Integer, Double> userRatingVector = usersOpinions.get(userIndex);
            // If user is not known, leave it
            if (userRatingVector == null) return;
            Map<Integer, Double> normalizedUV;
            final double userRatingsMean;
            if (isRanking) {
                // In case of ranking there is no need to normalize , values are only 0s and 1s
                normalizedUV = userRatingVector;
                userRatingsMean = 0;
            } else {
                // Normalize by user vector mean
                userRatingsMean = VectorUtils.mean(userRatingVector);
                normalizedUV = getNormalizedUserVector(userRatingVector, userRatingsMean);
            }


            // Compute similarities to each other user
            List<UserResult> potentialUsers = users
                    .stream()
                    .map(IdProvider::getExternalId)
                    .filter(u -> !Objects.equals(u, user.getExternalId()))
                    .map(u -> {
                        Map<Integer, Double> curUserVector = usersOpinions.get(addUserToIndex(u));
                        // User is not known , leave him/her alone
                        if (curUserVector == null) return null;
                        Map<Integer, Double> curUserRegVector;
                        double curUserRatingsMean = 0;

                        // Again normalize only in case of ratings, rankings are already "normalized"
                        if (isRanking) {
                            curUserRegVector = curUserVector;
                        } else {
                            curUserRatingsMean = VectorUtils.mean(curUserVector);
                            curUserRegVector = getNormalizedUserVector(curUserVector, curUserRatingsMean);
                        }

                        // Compute cosine similarity between users
                        double score = VectorUtils.dotProduct(normalizedUV, curUserRegVector) / (VectorUtils.euclideanNorm(normalizedUV) * VectorUtils.euclideanNorm(curUserRegVector));
                        return new UserResult(score, curUserRegVector);
                    })
                    .filter(Objects::nonNull)
                    .sorted(Collections.reverseOrder(Comparator.comparingDouble(i -> i.similarity)))
                    .collect(Collectors.toList());


            // Here we have computed similar users to current one and can recommend items.
            Map<Integer, Double> potentialItems = new HashMap<>(items.size());
            items.forEach(item -> {
                Integer intItemId = getItemId(item.getExternalId());
                AtomicDouble sumOfRegExternalOpinions = new AtomicDouble(0.0);
                AtomicDouble sumOfRegExternalUserScores = new AtomicDouble(0.0);

                long countOfRatings = potentialUsers
                        .stream()
                        .filter(i -> i.map.containsKey(intItemId) && i.similarity > 0)
                        .limit(neighborhoodSize)
                        .peek(potentialUser -> {
                            Map<Integer, Double> usersRatingMap = potentialUser.map;
                            // Weight is here reference users similarity to current user
                            double weight = potentialUser.similarity;
                            // In case of ratings normalize opinions
                            sumOfRegExternalOpinions.addAndGet(isRanking ? weight : weight * (usersRatingMap.get(intItemId)));
                            if (!isRanking()) {
                                sumOfRegExternalUserScores.addAndGet(weight);
                            }
                        })
                        .count();

                if (countOfRatings >= 2) {
                    double itemPredictedRating = isRanking()
                            ? sumOfRegExternalOpinions.get()
                            : userRatingsMean + (sumOfRegExternalOpinions.get() / sumOfRegExternalUserScores.get());
                    potentialItems.put(intItemId, itemPredictedRating);
                }
            });
            model.put(userIndex, potentialItems);

        });

        return model;
    }


    /**
     * Normalize user vector by its mean.
     */
    private Map<Integer, Double> getNormalizedUserVector(Map<Integer, Double> userRatingVector, double userMean) {
        return userRatingVector.entrySet().stream().collect(Collectors.toMap(Map.Entry::getKey, j -> j.getValue() - userMean));
    }

    /**
     * Util class keeps info about potential reference users
     */
    private static class UserResult {
        double similarity;
        Map<Integer, Double> map;

        public UserResult(double score, Map<Integer, Double> map) {
            this.similarity = score;
            this.map = map;
        }

    }


}
