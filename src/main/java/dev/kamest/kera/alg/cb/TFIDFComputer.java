package dev.kamest.kera.alg.cb;

import com.google.common.util.concurrent.AtomicDouble;
import dev.kamest.kera.alg.KeraComputer;
import dev.kamest.kera.alg.VectorUtils;
import dev.kamest.kera.model.IdProvider;
import dev.kamest.kera.model.KeraItem;
import dev.kamest.kera.model.KeraOpinion;
import dev.kamest.kera.model.KeraResultItem;
import dev.kamest.kera.model.internal.KeraMatrixModel;
import lombok.Setter;
import lombok.extern.apachecommons.CommonsLog;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Computes simplified TF-IDF algorithm
 * in which item relevance is computed as
 * <p>
 * term frequency * inverse document frequency
 * <p>
 * where TF is count of occurs in item
 * and IDF is log(countOfItems) - log(TF)
 * <p>
 * this TF-IDF vector then apply to each tagItem opinion value
 * and then divide value with euclidean distance for normalization of tag values.
 * <p>
 * In recommendations we build a user tag vector which means that
 * we builds vector of most relevant tags for this specific user.
 * This vector is build by history opinions where we get each opinion item and
 * for each tag we compute how much user "likes" items with this tag by summing opinion - mean.
 * <p>
 * Than we apply normalization by sqrt(sum(i*i)) of user vector and each item vector and
 * if we sum all user vector features that is presented in current reached item we can compute
 * then by thisSum / item normalization value / user normalization value compute final measurable
 * prediction of "liking" item by its features (tags)
 * <p>
 * <p>
 * Refs:
 * https://medium.com/@shengyuchen/how-tfidf-scoring-in-content-based-recommender-works-5791e36ee8da
 * https://www.researchgate.net/publication/287427252_Application_of_Content-Based_Approach_in_Research_Paper_Recommendation_System_for_a_Digital_Library
 *
 * <p>
 * This code is part of bachelor thesis:
 * Application of machine learning algorithms on purchase recommendation
 * UHK FIM KIT
 *
 * @author Štěpán Kamenik (kamenik.stepan@gmail.com / stepan.kamenik@uhk.cz), UHK FIM (c) 2020
 **/
@CommonsLog
public class TFIDFComputer extends KeraComputer {

    /**
     * Threshold wich indecates whether rating should be computed to user tag vector.
     */
    @Setter
    private Double ratingThreshold = 3.5d;
    /**
     * Indicates whether user tag vectors should be built weighted or not (user vector could contain some "noise")
     */
    @Setter
    private Boolean isWeighted = true;

    public TFIDFComputer(Boolean isRanking) {
        super(isRanking);
    }

    public TFIDFComputer(Boolean isRanking, Boolean isWeighted) {
        super(isRanking);
        this.isWeighted = isWeighted;
    }

    public static boolean needsRegisteredUsers() {
        return false;
    }

    /**
     * @see TFIDFComputer#recommend(java.lang.String, int, java.util.List, boolean)
     */
    @Override
    public List<KeraResultItem> recommend(String userId, int n, boolean forbidKnownItems) {
        return recommend(userId, n, Collections.emptyList(), forbidKnownItems);
    }

    /**
     * @see TFIDFComputer#recommend(java.lang.String, int, java.util.List, boolean, boolean)
     */
    @Override
    public List<KeraResultItem> recommend(String userId, int n, List<KeraItem> referenceItems, boolean forbidKnownItems) {
        return recommend(userId, n, referenceItems, forbidKnownItems, isWeighted);
    }

    /**
     * Builds user tag vector that is vector that explains how much user "likes"
     * tag in item, in other words "how many times" was tag in items user opinioned.
     * <p>
     * This vector is then used to compare with all items and tries to select one item
     * that has most common tags as user tag vector with higher measured values.
     * <p>
     * At the end is applied euclidean distance normalization to hold down "noise"
     * and normalize values that could be overhelmed.
     *
     * @param userId           user for whom we create recommendations
     * @param n                number of recommended items
     * @param referenceItems   items that should recommends be similar to.
     * @param forbidKnownItems indicates whether known (referenced and history items) could be recommended.
     * @param isWeighted       indicates whether user tag vectors should be weighted
     * @return recommandation list
     */
    public List<KeraResultItem> recommend(String userId, int n, List<KeraItem> referenceItems, boolean forbidKnownItems, boolean isWeighted) {
        List<KeraOpinion> opinions = opinionsProvider.getOpinionsFromUser(userId);

        if (opinions == null) return new LinkedList<>();

        List<KeraResultItem> results = new ArrayList<>();
        Map<Integer, Double> userVector = new HashMap<>();

        if (isWeighted && !isRanking()) {
            // Get mean of users opinions
            double mean = opinions.stream().mapToDouble(KeraOpinion::getOpinion).sum() / (double) opinions.size();
            opinions.forEach(i -> {
                Map<Integer, Double> itemVector = getModel().get(getItemId(i.getItemId()));
                for (Map.Entry<Integer, Double> e : itemVector.entrySet()) {

                    // Normalize value and apply mean
                    double weight = e.getValue() * (i.getOpinion() - mean);
                    if (userVector.containsKey(e.getKey())) {
                        // Add other posible cumulations
                        weight += userVector.get(e.getKey());
                    }
                    userVector.put(e.getKey(), weight);
                }
            });
        } else {

            // Gets only those opinions that are over threshold
            Stream<Map<Integer, Double>> mapStream = opinions
                    .stream()
                    .filter(r -> isRanking() || r.getOpinion() >= ratingThreshold)
                    .map(r -> getModel().get(getItemId(r.getItemId())));

            // Joins with reference items
            if (isRanking())
                mapStream = Stream.concat(mapStream, referenceItems.stream().map(i -> getModel().get(getItemId(i.getExternalId()))));

            // Computes user - tag vector
            mapStream
                    .filter(Objects::nonNull)
                    .flatMap(itemVector -> itemVector.entrySet().stream())
                    .forEach(e -> userVector.put(e.getKey(),
                            userVector.containsKey(e.getKey())
                                    ? e.getValue() + userVector.get(e.getKey())
                                    : e.getValue()));
        }

        // Computes user euclidean normalization value
        double userNorm = Math.sqrt(VectorUtils.sumOfSquares(userVector));


        List<String> forbiddenItems = getForbiddenItems(userId, referenceItems, forbidKnownItems);
        AtomicDouble max = new AtomicDouble();
        AtomicDouble min = new AtomicDouble();
        Map<String, KeraItem> items = itemsProvider
                .getObjects().stream().collect(Collectors.toMap(IdProvider::getExternalId, i->i));
        items
                .values()
                .stream()
                .filter(i -> !forbiddenItems.contains(i.getExternalId()))
                .map(keraItem -> getItemId(keraItem.getExternalId()))
                .filter(Objects::nonNull)
                .forEach(item -> {
                    Map<Integer, Double> itemVector = getModel().get(item);
                    double itemNorm = VectorUtils.sumOfSquares(itemVector);
                    if (itemNorm != 0.0 && userNorm != 0.0) {

                        // Computes item euclidean normalization value
                        itemNorm = Math.sqrt(itemNorm);

                        AtomicDouble userItem = new AtomicDouble(0.0);
                        userVector
                                .entrySet()
                                .stream()
                                .filter(j -> itemVector.containsKey(j.getKey()))
                                .forEach(j -> userItem.addAndGet(j.getValue() * itemVector.get(j.getKey())));

                        // Compute final score
                        double score = userItem.get() / (itemNorm * userNorm);
                        max.set(Math.max(score, max.get()));
                        min.set(Math.min(score, min.get()));
                        String itemId = getItemId(item);
                        results.add(new KeraResultItem(items.get(itemId), score));
                    }
                });

        // Normalize on range for evaluations
        final double finalMax = max.get();
        final double finalMin = min.get();
        List<String> usedMasters = new LinkedList<>();
        return results
                .stream()
                .distinct()
                .sorted(Collections.reverseOrder(Comparator.comparingDouble(KeraResultItem::getScore)))
                .peek(i -> i.setScore((5 / (finalMax - finalMin)) * (i.getScore() - finalMin)))
                .filter(i->{
                    // todo improve performance - contains on strings
                    String masterId = i.getMasterId();
                    if (masterId == null) return true;
                    if (usedMasters.contains(masterId)) return false;
                    usedMasters.add(masterId);
                    return true;
                })
                .limit(n)
                .collect(Collectors.toList());
    }


    /**
     * Computes TF-IDF (described in class doc)
     * and then aplies param to value with euclidean normalization.
     */
    @Override
    public KeraMatrixModel computeModel(Boolean isRanking) {

        Map<Integer, Double> df = new HashMap<>();
        Map<Integer, Map<Integer, Double>> itemVectors = new HashMap<>();

        // Compute TF vector
        List<KeraItem> items = itemsProvider.getObjects();
        items.forEach(item -> {
            Map<Integer, Double> tagSum = new HashMap<>();
            Set<Integer> addedTags = new HashSet<>();
            List<String> tags = Optional.ofNullable(item.getTags())
                    .orElse(Collections.emptyList());
            tags
                    .stream()
                    .map(this::addTagToIndex)
                    .forEach(tagIndex -> {
                        tagSum.put(tagIndex, tagSum.containsKey(tagIndex) ? tagSum.get(tagIndex) + 1.0 : 1.0);
                        if (!addedTags.contains(tagIndex)) {
                            addedTags.add(tagIndex);
                            df.put(tagIndex, df.containsKey(tagIndex) ? df.get(tagIndex) + 1.0 : 1.0);
                        }
                    });
            tagSum.entrySet().forEach(i->i.setValue(i.getValue() / tags.size()));
            itemVectors.put(addItemToIndex(item.getExternalId()), tagSum);
        });

        // Compute IDF
        df.entrySet().forEach(e -> e.setValue(Math.log(items.size() / e.getValue())));

        KeraMatrixModel modelData = new KeraMatrixModel();
        itemVectors.forEach((key, value) -> {
            Map<Integer, Double> tv = new HashMap<>(value);
            tv.entrySet().forEach(e -> e.setValue(e.getValue() * df.get(e.getKey())));

            // Normalize tag vec
            double sumSquares = VectorUtils.sumOfSquares(tv);
            double norm = Math.sqrt(sumSquares);
            tv.entrySet().forEach(e -> e.setValue(e.getValue() / norm));

            modelData.put(key, tv);
        });

        return modelData;
    }
}
