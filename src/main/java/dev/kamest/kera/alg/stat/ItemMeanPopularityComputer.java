package dev.kamest.kera.alg.stat;

import com.google.common.util.concurrent.AtomicDouble;
import dev.kamest.kera.alg.KeraComputer;
import dev.kamest.kera.model.IdProvider;
import dev.kamest.kera.model.KeraItem;
import dev.kamest.kera.model.KeraResultItem;
import dev.kamest.kera.model.internal.KeraMatrixModel;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Simple mean popularity item scoring algorithm.
 * <p>
 * In case of rating computes average mean rating of item.
 * In case of ranking computes popularity.
 *
 * @author Štěpán Kamenik (kamenik.stepan@gmail.com / stepan.kamenik@uhk.cz), UHK FIM (c) 2020
 * @implNote has only one dimension model {0, [x,y,z,......]}
 *
 * <p>
 * This code is part of bachelor thesis:
 * Application of machine learning algorithms on purchase recommendation
 * UHK FIM KIT
 **/
public class ItemMeanPopularityComputer extends KeraComputer {

    public ItemMeanPopularityComputer(Boolean isRanking) {
        super(isRanking);
    }

    public static boolean needsRegisteredUsers(){
        return false;
    }
    /**
     * Self explanatory - see class javadoc
     */
    @Override
    public List<KeraResultItem> recommend(String userId, int n, boolean forbidKnownItems) {
        return recommend(userId, n, Collections.emptyList(), forbidKnownItems);
    }

    /**
     * Self explanatory - see class javadoc
     */
    @Override
    public List<KeraResultItem> recommend(String userId, int n, List<KeraItem> referenceItems, boolean forbidKnownItems) {
        List<String> forbiddenItems = getForbiddenItems(userId, referenceItems, forbidKnownItems);

        Map<String, KeraItem> itemMap = itemsProvider.getObjects().stream().collect(Collectors.toMap(IdProvider::getExternalId, i -> i));

        List<String> usedMasters = new LinkedList<>();
        return getModel()
                .get(0)
                .entrySet()
                .stream()
                .sorted(Collections.reverseOrder(Comparator.comparingDouble(Map.Entry::getValue)))
                .map(i->new AbstractMap.SimpleEntry<>(getItemId(i.getKey()),i.getValue()))
                // filter out inactive products
                .filter(i->itemMap.containsKey(i.getKey()))
                .filter(i->itemMap.get(i.getKey())!=null)
                .map(i -> new KeraResultItem(itemMap.get(i.getKey()), i.getValue()))
                .filter(i -> {
                    if (!forbiddenItems.contains(i.getExternalId())){
                        String masterId = i.getMasterId();
                        if (masterId == null) return true;
                        if (usedMasters.contains(masterId)) return false;
                        usedMasters.add(masterId);
                        return true;
                    }
                    return false;
                })
                .limit(n)
                .collect(Collectors.toList());
    }

    /**
     * Self explanatory - see class javadoc
     */
    @Override
    public KeraMatrixModel computeModel(Boolean isRanking) {
        KeraMatrixModel model = new KeraMatrixModel();

        // Init opinion index
        Map<Integer, Map<Integer, Double>> opinions = new HashMap<>();
        opinionsProvider.getObjects().forEach(i -> {
            Integer itemIndex = addItemToIndex(i.getItemId());
            Map<Integer, Double> usersOps = opinions.getOrDefault(itemIndex, new HashMap<>());
            usersOps.put(addUserToIndex(i.getUserId()), i.getOpinion());
            opinions.put(itemIndex,usersOps);
        });

        Map<Integer,Double> means = new HashMap<>();
        opinions.forEach((itemId, opinionList) -> {
            // compute average rating or sum of ranking
            AtomicDouble sum = new AtomicDouble(0.0);
            opinionList.forEach((key, value) -> sum.addAndGet(value));
            means.put(itemId, isRanking() ? sum.get() : sum.get() / opinionList.size());
        });

        model.put(0,means);


        return model;
    }

}
