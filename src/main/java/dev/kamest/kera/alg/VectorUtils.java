package dev.kamest.kera.alg;

import com.google.common.util.concurrent.AtomicDouble;

import java.util.Map;

/**
 *  Util class for computing vectors in form of {@link Map}.
 *  Mostly is used {@link Map<Integer,Double>} and in this case Integer is inner Kera id of item or user
 *  and Double is rating or rank of item. In case of ranking, double signs counts of interactions from user to item.
 *
 *  This code is part of bachelor thesis:
 *  Application of machine learning algorithms on purchase recommendation
 *  UHK FIM KIT
 *
 *  @author Štěpán Kamenik (kamenik.stepan@gmail.com / stepan.kamenik@uhk.cz), UHK FIM (c) 2020
 *
 **/
public class VectorUtils {

    /**
     * Sums all entities of vector vec stored as {@link Double} in {@link Map}.
     *
     * @param vec Input vector from which sum is computed
     * @return sum of all entities in vector #vec
     */
    public static double sum(Map<Integer, Double> vec) {
        return vec.values().stream().mapToDouble(aDouble -> aDouble).sum();
    }

    /**
     * Computes sum of squared values in a vector vec.
     * @param vec Input vector from which sum is computed
     * @return sum of all entities in vector vec
     */
    public static double sumOfSquares(Map<Integer, Double> vec) {
        return vec.values().stream().mapToDouble(aDouble -> aDouble * aDouble).sum();
    }

    /**
     * Computes euclidean norm of a vector vec.
     * @param vec Input vector from which norm is computed
     * @return euclidean norm of vector vec
     */
    public static double euclideanNorm(Map<Integer, Double> vec) {
        return Math.sqrt(sumOfSquares(vec));
    }

    /**
     * Computes dot product of a vector #vecA and #vecB.
     * @param vecA Input vector from which product is computed
     * @param vecB Input vector from which product is computed
     * @return dot product of vectors
     */
    public static double dotProduct(Map<Integer, Double> vecA, Map<Integer, Double> vecB) {
        if (vecA.size() > vecB.size()) return dotProduct(vecB, vecA);

        return vecA.entrySet().stream().mapToDouble(a -> a.getValue() * vecB.getOrDefault(a.getKey(), 0.0)).sum();
    }

    /**
     * Computes global mean of a vector vec.
     * @param vec Input vector from which sum is computed
     * @return global mean of vector vec
     */
    public static double mean(Map<Integer, Double> vec) {
        return sum(vec) / vec.size();
    }

}
