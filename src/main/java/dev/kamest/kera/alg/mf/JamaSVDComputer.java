package dev.kamest.kera.alg.mf;

import Jama.Matrix;
import Jama.SingularValueDecomposition;
import com.google.common.util.concurrent.AtomicDouble;
import dev.kamest.kera.alg.KeraComputer;
import dev.kamest.kera.model.*;
import dev.kamest.kera.model.internal.KeraMatrixModel;
import lombok.Setter;
import lombok.extern.apachecommons.CommonsLog;
import org.apache.commons.math3.linear.ArrayRealVector;
import org.apache.commons.math3.linear.RealVector;

import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

/**
 * Computer that handles algorithm named SVD - Singular Value Decomposition.
 * In basics it decompose user-item rating matrix into 3 matrices
 * that can be dot producted and then creates back UI matrix.
 * This method has one big advantage, if we leave empty columns empty
 * latent factors created by decomposition could average blank spaces.
 * <p>
 * Another advantage is that product of decomposition is large number of latent features,
 * but if we truncate matrix and use only lets say 30 latent factors ,
 * the most relevant are still in the first 30 and we could fairly good still estimate results and
 * throw away users "noise".
 * <p>
 * When we trancate , matrices should look like:
 * (user matrix (users x latent), weights(latent x latent), item matrix (latent x items))
 * <p>
 * Then in recommendation we could only use dot product of
 * user vector ** weight (latent features) ** item vector
 * as result we receive estimated prediction.
 * <p>
 * In additional this implementation uses global,
 * user and item biases to reduce "noise" of users and normalize values.
 * <p>
 * Uses {@link SingularValueDecomposition} for decomposition itself.
 * <p>
 * <p>
 * Refs:
 * https://en.wikipedia.org/wiki/Singular-value_decomposition
 * https://arxiv.org/pdf/1803.03502.pdf
 * http://files.grouplens.org/papers/FnT%20CF%20Recsys%20Survey.pdf - page 103 +
 *
 * <p>
 * This code is part of bachelor thesis:
 * Application of machine learning algorithms on purchase recommendation
 * UHK FIM KIT
 *
 * @author Štěpán Kamenik (kamenik.stepan@gmail.com / stepan.kamenik@uhk.cz), UHK FIM (c) 2020
 **/
@CommonsLog
public class JamaSVDComputer extends KeraComputer {

    /**
     * Count of latent factors (Weight dimensions)
     */
    @Setter
    private int latentFactorCount = 25;


    public JamaSVDComputer(Boolean isRanking) {
        super(isRanking);
    }

    public static boolean needsRegisteredUsers() {
        return true;
    }

    /**
     * @see JamaSVDComputer#recommend(String, int, List, boolean)
     */
    @Override
    public List<KeraResultItem> recommend(String userId, int n, boolean forbidKnownItems) {
        return recommend(userId, n, Collections.emptyList(), forbidKnownItems);
    }

    /**
     * Recommending as result of dot product of vectors UV ** W ** IV.
     *
     * @param userId           user for whom we create recommendations
     * @param n                number of recommended items
     * @param referenceItems   items forbidden to be recommended.
     * @param forbidKnownItems indicates whether known (referenced and history items) could be recommended.
     * @return list of recommended items
     */
    @Override
    public List<KeraResultItem> recommend(String userId, int n, List<KeraItem> referenceItems, boolean forbidKnownItems) {

        Integer intUserId = getUserId(userId);
        // user not known
        if (intUserId == null) return Collections.emptyList();

        /// Get user vector from matrix
        double[] d = getUserMatrix().getArray()[intUserId];

        if (d == null) return Collections.emptyList();
        RealVector userVector = new ArrayRealVector(d);
        List<String> forbiddenItems = getForbiddenItems(userId, referenceItems, forbidKnownItems);


        List<KeraResultItem> results = new ArrayList<>();
        itemsProvider.getObjects().forEach(i -> {
            if (forbiddenItems.contains(i.getExternalId())) return;
            RealVector weights = new ArrayRealVector(getWeight());
            Integer itemId = getItemId(i.getExternalId());
            // Item not known
            if (itemId == null) return;
            // Get item vector from matrix
            RealVector itemVector = new ArrayRealVector(getItemMatrix().getArray()[itemId]);
            // Eval dot product of UV ** W ** IV
            double prediction = userVector.ebeMultiply(weights).dotProduct(itemVector);
            // Only in case of rating, normalize
            if (!isRanking())
                prediction += getGlobalBias() + getUserBias().get(intUserId) + getItemBias().get(itemId);
            results.add(new KeraResultItem(i, prediction));

        });

        List<String> usedMasters = new LinkedList<>();
        return results
                .stream()
                .sorted(Collections.reverseOrder(Comparator.comparingDouble(KeraResultItem::getScore)))
                .filter(i->{
                    String masterId = i.getMasterId();
                    if (masterId == null) return true;
                    if (usedMasters.contains(masterId)) return false;
                    usedMasters.add(masterId);
                    return true;
                })
                .limit(n)
                .collect(Collectors.toList());
    }

    @Override
    public KeraMatrixModel computeModel(Boolean isRanking) {
        // Is MF, we use overrided method dev.kamest.kera.alg.KeraComputer.computeExtModel
        return null;
    }

    /**
     * Prepares data - construct user-item matrix and push to apache SVD alg.
     * Then stores user matrix, item matrix and weight (latents)
     */
    @Override
    public void computeExtModel(Boolean isRanking) {
        // Init indexes
        if (!isRanking()) initBiases();
        Map<String, KeraUser> userIndex = usersProvider.getObjects().stream().collect(Collectors.toMap(IdProvider::getExternalId, i -> i));
        List<KeraItem> itemIndex = itemsProvider.getObjects();

        // Create user-item matrx
        Matrix matrix = Matrix.identity(userIndex.size(), itemIndex.size());
        opinionsProvider.getObjects()
                .stream()
                .filter(i -> !isRanking() || (userIndex.get(i.getUserId()) != null &&  userIndex.get(i.getUserId()).isRegistered()))
                .forEach(i -> {
                    int itemId = addItemToIndex(i.getItemId());
                    int userId = addUserToIndex(i.getUserId());
                    // Fill with normalized value from biases
                    // Again rankings are already "normalized"
                    double value = isRanking()
                            ? i.getOpinion()
                            : i.getOpinion() - getGlobalBias() - getUserBias().get(userId) - getItemBias().get(itemId);

                    matrix.set(userId, itemId, value);
                });

        // Construct SVD and then truncate to only count of latent factors stored in var latentFactorCount
        SingularValueDecomposition svd = new SingularValueDecomposition(matrix);
        Matrix um = svd.getU();
        Matrix im = svd.getV();

        // Truncate latent factors and store UM, IM, Weights
        setUserMatrix(um.getMatrix(0, um.getRowDimension() - 1, 0, latentFactorCount - 1));
        setItemMatrix(im.getMatrix(0, im.getRowDimension() - 1, 0, latentFactorCount - 1));
        setWeight(new ArrayRealVector(Arrays.stream(svd.getSingularValues()).limit(latentFactorCount).toArray()));
    }

    /**
     * Computes global, item and users biases.
     */
    public void initBiases() {
        AtomicDouble totalSum = new AtomicDouble(0);
        AtomicInteger totalCount = new AtomicInteger(0);

        // Init indexes and summarize opinions
        Map<Integer, List<Double>> itemIndex = new HashMap<>();
        Map<Integer, List<KeraOpinion>> usersOpinions = new HashMap<>();

        opinionsProvider.getObjects().forEach(i -> {
            Integer item = addItemToIndex(i.getItemId());
            List<Double> itemList = itemIndex.getOrDefault(item, new LinkedList<>());
            itemList.add(i.getOpinion());
            itemIndex.put(item, itemList);

            Integer userIndex = addUserToIndex(i.getUserId());
            List<KeraOpinion> opinionList = usersOpinions.getOrDefault(userIndex, new LinkedList<>());
            opinionList.add(i);
            usersOpinions.put(userIndex, opinionList);

            totalSum.addAndGet(i.getOpinion());
            totalCount.incrementAndGet();
        });

        // Compute global bias as sumOfAllOpinions/countOfAllOps
        setGlobalBias(totalCount.get() > 0 ? totalSum.get() / totalCount.get() : 0);

        // Compute item biases as sumOfItemOpinions/countOfItemOps - globalBleas
        itemIndex.forEach((k, v) -> getItemBias().put(k, v.stream().mapToDouble(i -> i).sum() / v.size() - getGlobalBias()));


        usersOpinions.forEach((u, v) -> {
            AtomicDouble usum = new AtomicDouble(0);
            //Computes user bias as sumOf(opinion-globalBias-itemBias) / countOfOpinions
            v.forEach(o -> usum.addAndGet(o.getOpinion() - getGlobalBias() - getItemBias().get(getItemId(o.getItemId()))));
            getUserBias().put(u, usum.get() / v.size());
        });
    }


}
