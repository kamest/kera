package dev.kamest.kera.evaluators;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public abstract class AbstractEvaluatorTest {

    @Test
    public abstract void testEvaluator() throws IOException;


    protected Map<String, Map<String, Double>> getThruthList(String filename) throws IOException {
        return (Map<String, Map<String, Double>>) new ObjectMapper().readValue(new File("data/evals/"+filename+".json"), HashMap.class);
    }

    protected Map<String, Map<String, Double>> getRecList(String filename) throws IOException {
        return (Map<String, Map<String, Double>>) new ObjectMapper().readValue(new File("data/evals/"+filename+".json"), HashMap.class);
    }
}
