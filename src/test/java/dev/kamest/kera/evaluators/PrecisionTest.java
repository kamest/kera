package dev.kamest.kera.evaluators;

import dev.kamest.kera.evaluators.precision.Precision;
import org.junit.Assert;

import java.io.IOException;

public class PrecisionTest extends AbstractEvaluatorTest {

    @Override
    public void testEvaluator() throws IOException {
        Evaluator evaluator = new Precision();
        evaluator.setTopN(10);

        double eval = evaluator.eval(getThruthList("test"), getRecList("rec"));
        Assert.assertEquals(0.3029692470837749, eval, 0.001);
    }
}
