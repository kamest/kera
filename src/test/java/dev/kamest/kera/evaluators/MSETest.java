package dev.kamest.kera.evaluators;

import dev.kamest.kera.evaluators.error.MSE;
import dev.kamest.kera.evaluators.precision.MAP;
import org.junit.Assert;

import java.io.IOException;

public class MSETest extends AbstractEvaluatorTest {

    @Override
    public void testEvaluator() throws IOException {
        Evaluator evaluator = new MSE();
        evaluator.setTopN(10);

        double eval = evaluator.eval(getThruthList("ratingTest"), getRecList("ratingRec"));
        Assert.assertEquals(0.9936934199461588, eval, 0.001);
    }
}
