package dev.kamest.kera.alg.mf;

import dev.kamest.kera.Kera;
import dev.kamest.kera.alg.AbstractKeraTest;
import dev.kamest.kera.alg.KeraComputer;
import dev.kamest.kera.alg.cb.TFIDFComputer;
import dev.kamest.kera.model.KeraOpinion;
import dev.kamest.kera.model.KeraResultItem;
import dev.kamest.kera.prov.ext.OpinionsProvider;
import lombok.extern.apachecommons.CommonsLog;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Just see test - data are manually computed or taken from Grouplens tests.
 *
 * Thanks https://grouplens.org/ for providing incredible datasets.
 * <p>
 * This code is part of bachelor thesis:
 * Application of machine learning algorithms on purchase recommendation
 * UHK FIM KIT
 *
 * @author Štěpán Kamenik (kamenik.stepan@gmail.com / stepan.kamenik@uhk.cz), UHK FIM (c) 2020
 **/
@CommonsLog
public class SVDComputerTest extends AbstractKeraTest {

    @Override
    protected KeraComputer getComputer(Kera kera) {
        JamaSVDComputer computer = new JamaSVDComputer(isRanking);
        computer.setNeighborhoodSize(20);
        return computer;
    }

    @Override
    protected String getComputerCode() {
        return "SVDTest";
    }

    @Override
    protected boolean needsRegisteredUsers() {
        return JamaSVDComputer.needsRegisteredUsers();
    }

    @Test
    public void testComputer(){
        initGlData();
        OpinionsProvider opinionsProvider = getOpinionsProvider();
        Kera kera = new Kera(getItemsProvider(),getUsersProvider(needsRegisteredUsers()), opinionsProvider);
        if (isComputeNewModel()) log.info("start training");
        JamaSVDComputer computer = new JamaSVDComputer(isRanking);
        computer.setNeighborhoodSize(20);
        if (isComputeNewModel()){
            kera.addComputerWithComputation("SVDTest",computer);
        }else {
            kera.addComputer("SVDTest",computer);
        }
        log.info("start recommending");
        long recStart = System.currentTimeMillis();
        List<KeraResultItem> resultItems = kera.recommend(
                "SVDTest",
                "320",
                10,
               true);

        log.info("Recommendation took: " + (System.currentTimeMillis() - recStart) + "ms" );


        Assert.assertEquals("318",resultItems.get(0).getExternalId());
        Assert.assertEquals(4.32233706109681,resultItems.get(0).getScore(),0.01);
        Assert.assertEquals("858",resultItems.get(1).getExternalId());
        Assert.assertEquals(4.320324713856657,resultItems.get(1).getScore(),0.01);
        Assert.assertEquals("4973",resultItems.get(2).getExternalId());
        Assert.assertEquals(4.259243916829215,resultItems.get(2).getScore(),0.01);
        Assert.assertEquals("1221", resultItems.get(3).getExternalId());
        Assert.assertEquals(4.245895750407865,resultItems.get(3).getScore(),0.01);
        Assert.assertEquals("1248", resultItems.get(4).getExternalId());
        Assert.assertEquals(4.223290984934304,resultItems.get(4).getScore(),0.01);
        Assert.assertEquals("7502", resultItems.get(5).getExternalId());
        Assert.assertEquals(4.211173056689106,resultItems.get(5).getScore(),0.01);

    }

    @Test
    public void testComputerSnData(){
        if (System.getenv("CI_SERVER") != null) return;
        initSNData();
        String computerCode = "SVDTestSn";

        OpinionsProvider opinionsProvider = getOpinionsProvider();
        Map<String,List<KeraOpinion>> ops = new HashMap<>();

        opinionsProvider.getObjects().forEach(i->{
            List<KeraOpinion> o = ops.getOrDefault(i.getUserId(),new LinkedList<>());
            o.add(i);
            ops.put(i.getUserId(), o);
        });

        Kera kera = new Kera(getItemsProvider(),getUsersProvider(needsRegisteredUsers()), opinionsProvider);
        if (isComputeNewModel()) log.info("start training");
        JamaSVDComputer computer = new JamaSVDComputer(isRanking);
        computer.setNeighborhoodSize(20);
        if (isComputeNewModel()){
            kera.addComputerWithComputation(computerCode,computer);
        }else {
            kera.addComputer(computerCode,computer);
        }
        log.info("start recommending");
        long recStart = System.currentTimeMillis();
        List<KeraResultItem> resultItems = kera.recommend(
                computerCode,
                "ffc64d83-db43-480e-ab80-80356f5fce01",
                10,
                true);

        log.info("Recommendation took: " + (System.currentTimeMillis() - recStart) + "ms" );

        Assert.assertEquals("070-0022-cis-barva-hewlet-v-hn-capucin-h-cis-leva-prava-v-prava-1",resultItems.get(0).getExternalId());
        Assert.assertEquals(0.06675730927832937,resultItems.get(0).getScore(),0.01);
        Assert.assertEquals("058-0137",resultItems.get(1).getExternalId());
        Assert.assertEquals(0.060251043492957575,resultItems.get(1).getScore(),0.01);
        Assert.assertEquals("058-0081",resultItems.get(2).getExternalId());
        Assert.assertEquals(0.0529623299747852,resultItems.get(2).getScore(),0.01);
        Assert.assertEquals("055-0013-cis-barva-best-1-v-wenge-best1", resultItems.get(3).getExternalId());
        Assert.assertEquals(0.04857732935676688,resultItems.get(3).getScore(),0.01);
        Assert.assertEquals("058-0133", resultItems.get(4).getExternalId());
        Assert.assertEquals(0.04790246705881899,resultItems.get(4).getScore(),0.01);
        Assert.assertEquals("058-0183", resultItems.get(5).getExternalId());
        Assert.assertEquals(0.047206624098569974,resultItems.get(5).getScore(),0.01);

    }
    @Test
    public void testComputerProData(){
        if (System.getenv("CI_SERVER") != null) return;
        initProData();
        String computerCode = "SVDTestPro";

        OpinionsProvider opinionsProvider = getOpinionsProvider();
        Kera kera = new Kera(getItemsProvider(),getUsersProvider(needsRegisteredUsers()), opinionsProvider);
        if (isComputeNewModel()) log.info("start training");
        JamaSVDComputer computer = new JamaSVDComputer(isRanking);
        computer.setNeighborhoodSize(20);
        if (isComputeNewModel()){
            kera.addComputerWithComputation(computerCode,computer);
        }else {
            kera.addComputer(computerCode,computer);
        }
        log.info("start recommending");
        long recStart = System.currentTimeMillis();
        List<KeraResultItem> resultItems = kera.recommend(
                computerCode,
                "d5da48a4-91e3-4be0-9f6f-504686be372d",
                10,
                true);

        log.info("Recommendation took: " + (System.currentTimeMillis() - recStart) + "ms" );

        Assert.assertEquals("8D702283-BDFE-46E8-B519-1C79AC5F8C7A",resultItems.get(0).getExternalId());
        Assert.assertEquals(0.4793534080151597,resultItems.get(0).getScore(),0.01);
        Assert.assertEquals("E044866C-FD69-4E61-900B-8A2BB7EF48C9",resultItems.get(1).getExternalId());
        Assert.assertEquals(0.43306805128888376,resultItems.get(1).getScore(),0.01);
        Assert.assertEquals("CE01BBC7-6739-4A7D-AF1E-9E56F82F217E",resultItems.get(2).getExternalId());
        Assert.assertEquals(0.4107965938607157,resultItems.get(2).getScore(),0.01);
        Assert.assertEquals("9C0FF489-1DB9-4104-8103-C140CFD11824", resultItems.get(3).getExternalId());
        Assert.assertEquals(0.3891196417888812,resultItems.get(3).getScore(),0.01);
        Assert.assertEquals("71F3A027-5CC2-4C2D-9866-B59661A1FE9C", resultItems.get(4).getExternalId());
        Assert.assertEquals(0.36591886991600875,resultItems.get(4).getScore(),0.01);
        Assert.assertEquals("6206031D-41A2-454D-8C07-BCAA3DBF6DE4", resultItems.get(5).getExternalId());
        Assert.assertEquals(0.36541446408904665,resultItems.get(5).getScore(),0.01);

    }
}
