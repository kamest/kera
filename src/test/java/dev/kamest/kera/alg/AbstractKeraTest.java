package dev.kamest.kera.alg;

import dev.kamest.kera.Kera;
import dev.kamest.kera.evaluators.Evaluator;
import dev.kamest.kera.evaluators.error.MAE;
import dev.kamest.kera.evaluators.error.MSE;
import dev.kamest.kera.evaluators.error.RMSE;
import dev.kamest.kera.evaluators.precision.MAP;
import dev.kamest.kera.evaluators.precision.Precision;
import dev.kamest.kera.evaluators.precision.RR;
import dev.kamest.kera.evaluators.precision.Recall;
import dev.kamest.kera.model.*;
import dev.kamest.kera.prov.ext.ItemsProvider;
import dev.kamest.kera.prov.ext.OpinionsProvider;
import dev.kamest.kera.prov.ext.UsersProvider;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.apachecommons.CommonsLog;
import org.junit.Test;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Collectors;

/**
 * Abstract test class for evaluation and testing computers ({@link KeraComputer}).
 *
 * Thanks https://grouplens.org/ for providing incredible datasets.
 * <p>
 * This code is part of bachelor thesis:
 * Application of machine learning algorithms on purchase recommendation
 * UHK FIM KIT
 *
 * @author Štěpán Kamenik (kamenik.stepan@gmail.com / stepan.kamenik@uhk.cz), UHK FIM (c) 2020
 **/
@CommonsLog
public abstract class AbstractKeraTest {

    /**
     * Syndicates whether computer in test should compute
     * new model or only recommend in case model is already saved on drive.
     */
    @Getter(AccessLevel.PROTECTED)
    @Setter(AccessLevel.PROTECTED)
    private boolean computeNewModel = false;
    /**
     * Syndicates if computers should compute ratings or rankings
     */
    protected boolean isRanking = true;

    // External data
    protected List<KeraItem> items = null;
    protected Map<String,String> masterIndex = new HashMap<>();
    protected List<KeraOpinion> opinions = null;
    protected List<KeraUser> users = null;
    private Map<String, List<String>> tags = null;

    /**
     * Provides computer for which test class is.
     * @param kera Kera service
     * @return Computer to be tested and evaluated.
     */
    protected abstract KeraComputer getComputer(Kera kera);

    /**
     * Provides computer unique code.
     * @return computers unique code.
     */
    protected abstract String getComputerCode();

    /**
     * Evaluates all provided computers on basic evaluators.
     */
    @Test
    public void evaluateSn() {
        // skip on ci
        if (System.getenv("CI_SERVER") != null) return;
        initSNData();
        isRanking = true;
        evaluate();
    }

    /**
     * Evaluates all provided computers on basic evaluators.
     */
    @Test
    public void evaluatePro() {
        // skip on ci
        if (System.getenv("CI_SERVER") != null) return;
        initProData();
        isRanking = true;
        evaluate();
    }

    @Test
    public abstract void testComputer();

    @Test
    public abstract void testComputerSnData();

    @Test
    public abstract void testComputerProData();

    /**
     * Evaluates all provided computers on basic evaluators.
     */
    @Test
    public void evaluateGlRank() {
        // skip on ci
        if (System.getenv("CI_SERVER") != null) return;
        initGlData();
        isRanking = true;
        evaluate();
    }

    /**
     * Evaluates all provided computers on basic evaluators.
     */
    @Test
    public void evaluateGlRating() {
        // skip on ci
        if (System.getenv("CI_SERVER") != null) return;
        initGlData();
        isRanking=false;
        evaluate();
    }

    /**
     * Evaluates all provided computers on basic evaluators.
     */
    public void evaluate() {
        UsersProvider usersProvider = getUsersProvider(needsRegisteredUsers());
        Map<String,KeraUser> userMap = usersProvider.getObjects().stream().collect(Collectors.toMap(IdProvider::getExternalId, i->i));
        List<KeraOpinion> objects = getOpinionsProvider().getObjects();
        objects = objects.stream().filter(i->!needsRegisteredUsers() || (userMap.get(i.getUserId()) != null && userMap.get(i.getUserId()).isRegistered())).collect(Collectors.toList());
        TestOpinionsProvider opinionsProvider = new TestOpinionsProvider(objects, true, 0.2);
        Kera kera = new Kera(getItemsProvider(), usersProvider, opinionsProvider);
        log.info("start training");
        KeraComputer computer = getComputer(kera);

        String computerCode = getComputerCode();
        if (computer == null) return;
        kera.addComputerWithComputation(computerCode + "eval", computer);
        log.info("start evaluating");

        List<KeraOpinion> testObjects = opinionsProvider.getTestObjects();

        Map<String,Map<String,Double>> testSet = new HashMap<>();
        Map<String,Map<String,Double>> recommendations = new HashMap<>();
        testObjects.forEach(i->{
            if (!needsRegisteredUsers() || (userMap.get(i.getUserId()) != null && userMap.get(i.getUserId()).isRegistered())){
                Map<String,Double> opinionList = testSet.getOrDefault(i.getUserId(), new HashMap<>());
                String itemOrMasterId = masterIndex.get(i.getItemId()) == null ? i.getItemId() : masterIndex.get(i.getItemId());
                opinionList.put(itemOrMasterId,i.getOpinion());
                testSet.put(i.getUserId(),opinionList);
            }
        });

        Map<String,Map<String,Double>> collect = testSet.entrySet()
                .stream()
                .filter(stringDoubleMap -> !needsRegisteredUsers() || (userMap.get(stringDoubleMap.getKey()) != null && userMap.get(stringDoubleMap.getKey()).isRegistered()))
                .distinct()
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));

        AtomicLong sumTime = new AtomicLong(0);
        AtomicInteger count = new AtomicInteger(0);
        int collectSize = collect.size();
        collect.keySet()
                .forEach(i->{
                    if (count.get()%100==0) log.info("recommended : " +count.get() + " from: " + collectSize + " , actual average is: " + (count.get()== 0 ? 0: sumTime.get()/count.get()));
                    long time = System.currentTimeMillis();
                    List<KeraResultItem> recommend = computer.recommend(i, 300, false);
                    recommendations.put(i,recommend.stream().collect(Collectors.toMap(k->k.getMasterId() == null? k.getExternalId() : k.getMasterId(), KeraResultItem::getScore)));
                    sumTime.addAndGet(System.currentTimeMillis()-time);
                    count.incrementAndGet();
        });
        log.info("average recommendation time was: " + sumTime.get() / count.get() );

        List<Evaluator> evals = isRanking
                ? Arrays.asList(new MAP(),new Precision(),new Recall(),new RR())
                : Arrays.asList(new MAE(),new MSE(),new RMSE(), new MAP(),new Precision(),new Recall(),new RR());
        evals.forEach(i->{
            for (int j = 10; j <= 100; j+=10) {
                try{
                    i.setTopN(j);
                    double eval = i.eval(testSet, recommendations);
                    log.info(i.getClass().getSimpleName() +" (topK: "+j+"): " + eval);
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        });

    }

    protected abstract boolean needsRegisteredUsers();


    /**
     * Provide items from dataset.
     */
    protected ItemsProvider getItemsProvider() {
        return () -> items.stream().distinct().collect(Collectors.toList());
    }

    /**
     * Provide users from dataset.
     */
    protected UsersProvider getUsersProvider(boolean onlyRegistered) {
        return new UsersProvider(onlyRegistered) {
            @Override
            public List<KeraUser> getObjects() {
                return users.stream().filter(user->!isOnlyRegistered()||user.isRegistered()).collect(Collectors.toList());
            }
        };
    }

    /**
     * Provide opinions from dataset.
     */
    protected OpinionsProvider getOpinionsProvider() {
        return new OpinionsProvider() {
            @Override
            public List<KeraOpinion> getOpinionsFromUser(String userId) {
                return opinions
                        .stream()
                        .filter(i -> i.getUserId().equals(userId))
                        .collect(Collectors.toList());
            }

            @Override
            public List<KeraOpinion> getObjects() {
                return opinions;
            }
        };
    }

    /**
     * Deserialize data from drive which contains https://grouplens.org/ data about movie ratings.
     */
    protected void initGlData(){
        items = new LinkedList<>();
        users = new LinkedList<>();
        opinions = new LinkedList<>();
        tags = new HashMap<>();

        isRanking=false;
        try (BufferedReader br = new BufferedReader(new FileReader("data/matrices/ml-100k/ratings.csv"))) {
            String line;
            int count = 0;
            while ((line = br.readLine()) != null) {
                if (count!=0){
                    String[] values = line.split(",");
                    String userId = values[0];
                    String itemId = values[1];
                    String opinion = values[2];
                    items.add(new KeraItem(itemId));
                    KeraUser user = new KeraUser(userId);
                    users.add(user);
                    opinions.add(new KeraOpinion(userId,itemId,Double.parseDouble(opinion)));
                }
                count++;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        try (BufferedReader br = new BufferedReader(new FileReader("data/matrices/ml-100k/tags.csv"))) {
            String line;
            int count = 0;
            while ((line = br.readLine()) != null) {
                if (count!=0){
                    String[] values = line.split(",");
                    String itemId = values[0];
                    String tag = values[2].replace("\"","");
                    List<String> itemTags = tags.getOrDefault(itemId,new LinkedList<>());
                    itemTags.add(tag);
                    tags.put(itemId, itemTags);
                }
                count++;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        log.info("retrieving completed, distincting");
        opinions= opinions.stream().distinct().collect(Collectors.toList());
        users=users.stream().distinct().collect(Collectors.toList());
        items=items.stream().distinct().peek(i-> Optional.ofNullable(tags.get(i.getExternalId())).ifPresent(j->i.getTags().addAll(j))).collect(Collectors.toList());

    }

    /**
     * Deserialize data from drive which contains anonymized production data about product bought.
     */
    protected void initSNData() {
        items = new LinkedList<>();
        users = new LinkedList<>();
        opinions = new LinkedList<>();
        tags = new HashMap<>();

        List<String> itemCodes = new LinkedList<>();
        isRanking = true;
        try (BufferedReader br = new BufferedReader(new FileReader("data/matrices/sn/ranks.csv"))) {
            String line;
            int count = 0;
            while ((line = br.readLine()) != null) {
                if (count != 0 && count < 100000) {
                    String[] values = line.split(",");
                    String userId = values[0];
                    String itemId = values[1];
                    String opinion = values[2];
                    String registered = values[3];
                    itemCodes.add(itemId);
                    users.add(new KeraUser(userId,registered.equals("1")));
                    KeraOpinion opinion1 = new KeraOpinion(userId, itemId, Double.parseDouble(opinion));
                    opinions.add(opinion1);
                }
                count++;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        try (BufferedReader br = new BufferedReader(new FileReader("data/matrices/sn/tags.csv"))) {
            String line;
            int count = 0;
            while ((line = br.readLine()) != null) {
                if (count!=0){
                    String[] values = line.split(",");
                    String itemId = values[0];
                    String tag = values[1];
                    List<String> itemTags = tags.getOrDefault(itemId,new LinkedList<>());
                    itemTags.add(tag);
                    tags.put(itemId, itemTags);
                }
                count++;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        Map<String, KeraItem> rawItemMap = new HashMap<>();

        try (BufferedReader br = new BufferedReader(new FileReader("data/matrices/sn/productInfo.csv"))) {
            String line;
            int count = 0;
            while ((line = br.readLine()) != null) {
                if (count != 0) {
                    String[] values = line.split(",");
                    String itemName = values[0];
                    String itemId = values[1];
                    String optionalMasterId = values.length > 2 ? values[2] : null;
                    if (optionalMasterId != null) masterIndex.put(itemId,optionalMasterId);
                    rawItemMap.put(itemId, new KeraItem(itemId, Optional.ofNullable(tags.get(itemId)).orElse(Collections.emptyList()), itemName, optionalMasterId));
                }
                count++;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        log.info("retrieving completed, distincting");
        users = users.stream().distinct().collect(Collectors.toList());
        // filter only relevant items
        items = itemCodes.stream().distinct().map(rawItemMap::get).filter(Objects::nonNull).collect(Collectors.toList());
        opinions = opinions.stream().filter(i -> rawItemMap.containsKey(i.getItemId())).distinct().collect(Collectors.toList());

    }

    /**
     * Deserialize data from drive which contains anonymized production data about product bought.
     */
    protected void initProData() {
        items = new LinkedList<>();
        users = new LinkedList<>();
        opinions = new LinkedList<>();
        tags = new HashMap<>();

        List<String> itemCodes = new LinkedList<>();
        isRanking = true;
        try (BufferedReader br = new BufferedReader(new FileReader("data/matrices/pro/pro_ranks_converted.csv"))) {
            String line;
            int count = 0;
            while ((line = br.readLine()) != null) {
                if (count != 0 && count < 100000) {
                    String[] values = line.split(",");
                    String userId = values[0];
                    String itemId = values[1];
                    String opinion = values[2];
                    String registered = values[3];
                    itemCodes.add(itemId);
                    users.add(new KeraUser(userId, registered.equals("1")));
                    KeraOpinion opinion1 = new KeraOpinion(userId, itemId, Double.parseDouble(opinion));
                    opinions.add(opinion1);
                }
                count++;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        Map<String, KeraItem> rawItemMap = new HashMap<>();

        try (BufferedReader br = new BufferedReader(new FileReader("data/matrices/pro/proTags.csv"))) {
            String line;
            int count = 0;
            while ((line = br.readLine()) != null) {
                if (count!=0){
                    String[] values = line.split(",");
                    String itemId = values[0];
                    String tag = values[1];
                    List<String> itemTags = tags.getOrDefault(itemId,new LinkedList<>());
                    itemTags.add(tag);
                    tags.put(itemId, itemTags);
                }
                count++;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }


        log.info("retrieving completed, distincting");
        users = users.stream().distinct().collect(Collectors.toList());
        // filter only relevant items
        items = itemCodes.stream().distinct().map(i->new KeraItem(i,Optional.ofNullable(tags.get(i)).orElse(Collections.emptyList()),null,null)).collect(Collectors.toList());
        opinions = opinions.stream().distinct().collect(Collectors.toList());

    }

    /**
     * Provider that splits data into test and training set,
     * often this split is 20 % test and 80% training.
     *
     * Sometimes is used so called cross-validation test,
     * where data are evaluated in many turns with these data sets:
     * 1 turn > Train(20%), Train(20%), Train(20%), Train(20%), Test(20%)
     * 2 turn > Train(20%), Train(20%), Train(20%), Test(20%), Train(20%)
     * 3 turn > Train(20%), Train(20%), Test(20%), Train(20%), Train(20%)
     * 4 turn > Train(20%), Test(20%), Train(20%), Train(20%), Train(20%)
     * 5 turn > Test(20%), Train(20%), Train(20%), Train(20%), Train(20%)
     * But this is not implemented yet, maybe in next version.
     * This alg has one great advantage that computer cannot over-train test set.
     */
    public static class TestOpinionsProvider implements OpinionsProvider {
        List<KeraOpinion> trainSet;
        Map<String,List<KeraOpinion>> trainMap = new HashMap<>();
        List<KeraOpinion> testSet;

        public TestOpinionsProvider(List<KeraOpinion> objects, boolean isTest, double testRatio) {
            if (isTest){
                int size = objects.size();
                int trainSize = (int) Math.ceil(size * (1 - testRatio));
                Collections.shuffle(objects);
                trainSet = objects.stream().skip(trainSize/2).limit(trainSize).collect(Collectors.toList());
                int testSize = size - trainSize;
                testSet = objects.subList(testSize, objects.size());
            }else {
                trainSet = objects;
                trainSet = Collections.emptyList();
            }
            trainSet.forEach(i->{
                List<KeraOpinion> opinions = trainMap.getOrDefault(i.getUserId(), new LinkedList<>());
                opinions.add(i);
                trainMap.put(i.getUserId(),opinions);
            });
        }


        @Override
        public List<KeraOpinion> getObjects() {
            return trainSet;
        }

        public List<KeraOpinion> getTestObjects() {
            return testSet;
        }

        @Override
        public List<KeraOpinion> getOpinionsFromUser(String userId) {
            return trainMap.get(userId);
        }
    }



}
