package dev.kamest.kera;

import dev.kamest.kera.alg.AbstractKeraTest;
import dev.kamest.kera.alg.KeraComputer;
import dev.kamest.kera.model.KeraItem;
import dev.kamest.kera.model.KeraOpinion;
import dev.kamest.kera.model.KeraUser;
import org.junit.Ignore;
import org.junit.Test;

import java.io.*;
import java.util.LinkedList;
import java.util.UUID;
import java.util.stream.Collectors;

/**
 * Anonymize data
 *
 * <p>
 * This code is part of bachelor thesis:
 * Application of machine learning algorithms on purchase recommendation
 * UHK FIM KIT
 *
 * @author Štěpán Kamenik (kamenik.stepan@gmail.com / stepan.kamenik@uhk.cz), UHK FIM (c) 2020
 **/
@Ignore
public class ConvertUid extends AbstractKeraTest {

    @Test
    public void convertUID(){
        items=new LinkedList<>();
        users=new LinkedList<>();
        opinions=new LinkedList<>();

        try (BufferedReader br = new BufferedReader(new FileReader("data/matrices/ext/pro_ranking_matrix_unANONYMIZE_.csv"))) {
            String line;
            while ((line = br.readLine()) != null) {
                String[] values = line.split(",");
                String userId = values[0];
                String itemId = values[1];
                String opinion = values[2];
                String registered = values[3];
                if (!userId.contains("3dvo.cz") && !userId.contains("test") ){
                    items.add(new KeraItem(itemId));
                    users.add(new KeraUser(userId, registered.equals("1")));
                    opinions.add(new KeraOpinion(userId,itemId,Double.parseDouble(opinion)));
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        users = users.stream().distinct().collect(Collectors.toList());
        items = items.stream().distinct().collect(Collectors.toList());
        opinions = opinions.stream().distinct().collect(Collectors.toList());

        File csvOutputFile = new File("data/matrices/ext/pro_ranks_converted.csv");
        try (PrintWriter pw = new PrintWriter(csvOutputFile)) {
            for (KeraUser user : users) {
                UUID uuid = UUID.randomUUID();
                opinions.stream().filter(i->i.getUserId().equals(user.getExternalId())).forEach(i->{
                    pw.println(uuid.toString()+","+i.getItemId()+","+i.getOpinion() +"," + (user.isRegistered() ?"1":"0"));
                });
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected KeraComputer getComputer(Kera kera) {
        return null;
    }

    @Override
    protected String getComputerCode() {
        return null;
    }

    @Override
    public void testComputer() {

    }

    @Override
    public void testComputerSnData() {

    }

    @Override
    public void testComputerProData() {

    }

    @Override
    protected boolean needsRegisteredUsers() {
        return false;
    }
}
